package es.upm.dit.apsv.traceconsumer2;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import org.springframework.context.annotation.Bean;
import java.util.function.Consumer;
import es.upm.dit.apsv.traceconsumer2.model.TransportationOrder;

import es.upm.dit.apsv.traceconsumer2.Repository.TraceRepository;
import es.upm.dit.apsv.traceconsumer2.Repository.TransportationOrderRepository;
import es.upm.dit.apsv.traceconsumer2.model.TraceOrder;

import java.util.Optional;


@SpringBootApplication
public class Traceconsumer2Application {


	public static final Logger log = LoggerFactory.getLogger(TraceRepository.class);
	
	@Autowired
	private TraceRepository traceRep;
	
	@Autowired
	private TransportationOrderRepository transpOrder;

	public static void main(String[] args) {
		SpringApplication.run(Traceconsumer2Application.class, args);
	}

	@Bean("consumer")
		public Consumer<TraceOrder> checkTrace() {
			return t -> {
				t.setTraceId(t.getTruck() + t.getLastSeen());
				traceRep.save(t);
				Optional<TransportationOrder> result = transpOrder.findById(t.getTruck());				
				if (result.isPresent()) {
					TransportationOrder order = result.get();
					order.setLastDate(t.getLastSeen());
					order.setLastLat(t.getLat());
					order.setLastLong(t.getLng());
					if (order.distanceToDestination() < 10){
						order.setSt(1);
					}
					transpOrder.save(order);
					log.info("Order updated: " + result);
				}else{
					// log.info("The requested order wasn't found");
				}
			};
		}
}
