package es.upm.dit.apsv.traceconsumer2.Repository;

import org.springframework.data.repository.CrudRepository;

import es.upm.dit.apsv.traceconsumer2.model.TraceOrder;
import org.springframework.context.annotation.Bean;
import java.util.function.Consumer;


public interface TraceRepository extends CrudRepository<TraceOrder, String> {
}
